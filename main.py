# Chosen frame feature exraction and bject tracking algorithm
# 
# Burak KURT
# hsynbrkkrt@gmail.com

from imutils.video import VideoStream
import imutils
import time
import numpy as np
import cv2

def tracker_release():
    if tracker_type == 'BOOSTING':
        tracker = cv2.TrackerBoosting_create()   # 3 channel 
    if tracker_type == 'MIL':
        tracker = cv2.TrackerMIL_create()
    if tracker_type == 'KCF':
        tracker = cv2.TrackerKCF_create()
    if tracker_type == 'TLD':
        tracker = cv2.TrackerTLD_create()
    if tracker_type == 'MEDIANFLOW':
        tracker = cv2.TrackerMedianFlow_create()
    if tracker_type == 'GOTURN':
        tracker = cv2.TrackerGOTURN_create()
    if tracker_type == 'MOSSE':
        tracker = cv2.TrackerMOSSE_create()
    if tracker_type == "CSRT":
        tracker = cv2.TrackerCSRT_create()
    
    return tracker

if __name__ == "__main__": 

    (major, minor) = cv2.__version__.split(".")[:2]

    tracker_types = ['BOOSTING','MIL','KCF','TLD', 'MEDIANFLOW', 'MOSSE', 'CSRT']
    tracker_type = tracker_types[6]

    if int(major)==3 and int(minor) < 3:
        tracker = cv2.Tracker_create(tracker_type)
    else:
        tracker = tracker_release()
    
    initbbox = None
    box_old = None
    search_bbox = [0,0,0,0]
    search_scale = 2

    #video = cv2.VideoCapture('./session0_center.avi')
    #video = cv2.VideoCapture('./teknofest_1.mp4') 
    #video = cv2.VideoCapture('./teknofest_2.mp4')
    #video = cv2.VideoCapture('./teknofest_3.mp4')
    #video = cv2.VideoCapture('./video0.mkv')
    video = cv2.VideoCapture('./car-overhead-1.avi')
    #video = cv2.VideoCapture('./car-overhead-3.avi')
    
    if (video.isOpened == False):
        print(" Error opening video stream or file")
        sys.exit()

    while True:
        ok, frame = video.read()
        if not ok:
            print ('Cannot read video file')
            break

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = imutils.resize(frame, width=1000,
                                      height=1000)

        key = cv2.waitKey(1) & 0xFF
        if key == ord("s"):
            # select the bounding box of the object we want to track (make
            # sure you press ENTER or SPACE after selecting the ROI)
            initbbox = cv2.selectROI("Frame", frame, fromCenter=False, 
                                                     showCrosshair=True)
            box_old = initbbox

            # w + 2 / h + 2, in order to solve zero heigt and weight problem
            tracker.init(frame, (initbbox[0], initbbox[1], initbbox[2]+2, initbbox[3]+2))
            orb = cv2.ORB_create()
            bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck = True)


        elif key == ord("a"):
            # Reinitialize tracker, if tracker lost 
            print("HERE")
            tracker = tracker_release()

        if initbbox is not None:
            # grab the new bounding box coordinates of the object
            timer = cv2.getTickCount()
            success, box = tracker.update(frame)

            search_bbox[0] = (box[0] + box[2]/2) - (search_scale * box[2])/2
            search_bbox[1] = (box[1] + box[3]/2) - (search_scale * box[3])/2
            search_bbox[2] = search_scale * box[2]
            search_bbox[3] = search_scale * box[3]

            # check to see if the tracking was a success
            if success:
                (x, y, w, h) = [int(v) for v in box]
                
                print("box", x,y,w,h)
                x = int(box_old[0]) if x < 0 else x
                y = int(box_old[1]) if y < 0 else y
                w = int(box_old[2]) if w < 0 else w
                h = int(box_old[3]) if h < 0 else h
                print("box1", x,y,w,h)

                box_old = (x, y, w, h)    

                # Show tracking objct  
                # tracked_object = frame[y:y+h, x:x+w]
                tracked_object = frame[int(box_old[1]-10):int(box_old[1]+box_old[3]+20), int(box_old[0]-10):int(box_old[0]+box_old[2]+20)]
                search_area = frame[int(search_bbox[1]):int(search_bbox[1]+search_bbox[3]), int(search_bbox[0]):int(search_bbox[0]+search_bbox[2])]

                if tracked_object is not None:
                    #cv2.imshow("tacked object", tracked_object) # there are assertion error there !!!
                    #cv2.imshow("search area", search_area)
                    pass

                cv2.imshow("tacked object", tracked_object) # there are assertion error there !!!
                cv2.imshow("search area", search_area)

                # ORB
                obj_keypoints, obj_descriptor = orb.detectAndCompute(tracked_object, None)
                search_keypoints, search_descriptor = orb.detectAndCompute(search_area, None)

                matches = bf_matcher.match(obj_descriptor, search_descriptor)
                matches = sorted(matches, key=lambda x : x.distance)
                #img3 = cv2.drawMatches(tracked_object,obj_keypoints,
                #                       search_area,search_keypoints, matches[:10], search_area, flags=2)
                #cv2.imshow("img3",img3)

                #list_kp1 = [obj_keypoints[mat.queryIdx].pt for mat in matches[:10]] 
                list_kp2 = [search_keypoints[mat.trainIdx].pt for mat in matches[:10]]
                if len(list_kp2) is not 0:
                    mean_kp2 = np.mean(list_kp2, axis=0)
                    cv2.rectangle(search_area, (int(mean_kp2[0]), int(mean_kp2[1])), (int(mean_kp2[0])+2, int(mean_kp2[1])+2), (255,0,0), 2, 1)
                    cv2.imshow("search area", search_area)
                else:
                    print("Feature cannot found !!!")
            else:
                # image detection will do
                # Reinitialize tracker, if tracker lost
                tracker = tracker_release()

            # update the FPS counter
            fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

            # Draw Bounding Box
            if success:
                p1 = (int(x), int(y))
                p2 = (int(x + w), int(y + h))
                cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
                cv2.rectangle(frame, (int(search_bbox[0]), int(search_bbox[1])), (int(search_bbox[0]+search_bbox[2]), int(search_bbox[1]+search_bbox[3])), (0,0,255), 2, 1)     
            else:
               cv2.putText(frame, "Tracking failure detected", (100, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,255), 2)
            
            # Display tracker type on frame
            cv2.putText(frame, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)
        
            # Display FPS on frame
            cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

        # show the output frame
        cv2.imshow("Frame", frame)

        # Exit if ESC pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break